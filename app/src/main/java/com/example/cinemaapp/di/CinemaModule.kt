package com.example.cinemaapp.di

import com.example.cinemaapp.model.CinemaService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class CinemaModule {


    @Provides
    @Singleton
    fun providesCinemaService(): CinemaService = Retrofit.Builder()
        .baseUrl(CinemaService.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CinemaService::class.java)

}