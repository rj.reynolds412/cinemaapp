package com.example.cinemaapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.cinemaapp.databinding.FragmentCinemaSearchBinding
import com.example.cinemaapp.util.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CinemaSearchFragment : Fragment() {

    private var _binding: FragmentCinemaSearchBinding? = null
    private val binding get() = _binding!!


    private val queryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            if (query != null) {
                findNavController()
                    .navigate(
                        CinemaSearchFragmentDirections.actionCinemaSearchFragmentToCinemaFragment(
                            cinema = query))
            }
            hideKeyboard()
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            binding.tvNoQuery.isVisible = true
            return true
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentCinemaSearchBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }


    private fun initViews() = with(binding) {
        svSearch.setOnQueryTextListener(queryTextListener)

        /* fbSearch.setOnClickListener {
             svSearch.run {
                 clearFocus();
             }
         }*/

    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}