package com.example.cinemaapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.cinemaapp.databinding.FragmentCinemaDetailBinding
import com.example.cinemaapp.model.response.CinemaDetailResponse
import com.example.cinemaapp.viewModel.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CinemaDetailFragment : Fragment() {

    private var _binding: FragmentCinemaDetailBinding? = null
    private val binding get() = _binding!!
    private val detailViewModel by viewModels<DetailViewModel>()
    private val args by navArgs<CinemaDetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentCinemaDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()

    }

    private fun initObservers() = with(binding) {
       val details = args.imbdId
        detailViewModel.getDrinkDetailsById(details)
        detailViewModel.detail.observe(viewLifecycleOwner){ state ->
            val details : CinemaDetailResponse = state.details
            displayDetails(details)
        }
    }


    private fun displayDetails(details: CinemaDetailResponse) = with(binding){
       details.run {
        ivDetail.load(details.poster)
           tvCinemaTitle.text = title
           val detail = listOfNotNull(
               genre,
               rated,
               released,
               runtime,
               actors,
               director,
               writer,
               imdbRating,
               imdbVotes,
               plot
           )
           tvDetails.text = detail.joinToString("\n")
       }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}