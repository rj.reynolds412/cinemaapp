package com.example.cinemaapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.cinemaapp.adapter.CinemaAdapter
import com.example.cinemaapp.databinding.FragmentCinemaBinding
import com.example.cinemaapp.model.response.CinemaDetailResponse
import com.example.cinemaapp.model.response.Search
import com.example.cinemaapp.viewModel.CinemaViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CinemaFragment : Fragment() {
    private var _binding: FragmentCinemaBinding? = null
    private val binding get() = _binding!!
    private val cinemaViewModel by viewModels<CinemaViewModel>()
    private val cinemaAdapter by lazy { CinemaAdapter(::movieClicked) }

    private fun movieClicked(cinema: Search) {
        val nav =
            CinemaFragmentDirections.actionCinemaFragmentToCinemaDetailFragment(imbdId = cinema.imdbID)
        findNavController().navigate(nav)

    }

    private val args by navArgs<CinemaFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentCinemaBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() = with(binding) {
        rvCinema.adapter = cinemaAdapter
        val cinema = args.cinema
        cinemaViewModel.getCinema(cinema)
        cinemaViewModel.cinema.observe(viewLifecycleOwner) { state ->
            cinemaAdapter.displayCinema(state.cinema)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}