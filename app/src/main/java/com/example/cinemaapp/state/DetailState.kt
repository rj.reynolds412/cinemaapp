package com.example.cinemaapp.state

import com.example.cinemaapp.model.response.CinemaDetailResponse
import com.example.cinemaapp.model.response.Search

data class DetailState(
    val isLoading: Boolean = false,
    val details: CinemaDetailResponse=  CinemaDetailResponse(),
    val error: String = "You Know You Messed Up Right?",
)