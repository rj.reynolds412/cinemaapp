package com.example.cinemaapp.state

import com.example.cinemaapp.model.response.CinemaResponse
import com.example.cinemaapp.model.response.Search

data class CinemaState(
   val isLoading: Boolean = false,
   val cinema : List<Search> = emptyList(),
   val error : String = "You Screwed Up"
)
