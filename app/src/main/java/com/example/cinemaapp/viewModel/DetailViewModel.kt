package com.example.cinemaapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cinemaapp.model.CinemaRepo
import com.example.cinemaapp.state.DetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val repo: CinemaRepo) : ViewModel() {

    private val _detail = MutableLiveData(DetailState(isLoading = true))
    val detail: LiveData<DetailState> get() = _detail

    fun getDrinkDetailsById(detail: String) {
        viewModelScope.launch {
            _detail.value = DetailState(isLoading = true)
            val details = repo.getCinemaDetailById(detail)
            if (details.response.toBoolean()) {
                _detail.value = DetailState(isLoading = false, details = details)
            } else {
                _detail.value = DetailState(isLoading = false, error = details.error)
            }
        }
    }
}