package com.example.cinemaapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cinemaapp.model.CinemaRepo
import com.example.cinemaapp.state.CinemaState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CinemaViewModel @Inject constructor(private val repo: CinemaRepo) : ViewModel() {

    private var _cinema: MutableLiveData<CinemaState> =
        MutableLiveData(CinemaState(isLoading = true))
    val cinema: LiveData<CinemaState> get() = _cinema

    fun getCinema(cinema: String) {
        viewModelScope.launch {
            _cinema.value = CinemaState(isLoading = true)
            val result = repo.getCinema(cinema)
            if (result.response.toBoolean()) {
                _cinema.value = CinemaState(isLoading = false, cinema = result.search)
            } else {
                _cinema.value = CinemaState(isLoading = false, error = result.error)
            }
        }
    }

}