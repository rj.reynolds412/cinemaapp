package com.example.cinemaapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cinemaapp.databinding.ItemCinemaBinding
import com.example.cinemaapp.model.response.CinemaDetailResponse
import com.example.cinemaapp.model.response.CinemaResponse
import com.example.cinemaapp.model.response.Search

class CinemaAdapter(val movieClicked: (cinema: Search) -> Unit) : RecyclerView.Adapter<CinemaAdapter.CinemaViewHolder>() {
    private var cinema = mutableListOf<Search>()

    class CinemaViewHolder(private val binding: ItemCinemaBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun getCinema(cinema: Search) = with(binding) {
            cinema.run {
                ivCinema.load(cinema.poster)
                val details = "$title\n$year\n$type"
                tvCinema.text = details
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCinemaBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CinemaViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CinemaViewHolder {
        return CinemaViewHolder.getInstance(parent).apply {
            itemView.setOnClickListener { movieClicked(cinema[adapterPosition]) }
        }
    }

    override fun onBindViewHolder(holder: CinemaViewHolder, position: Int) {
        val cinema = cinema[position]
        holder.getCinema(cinema)
    }

    override fun getItemCount(): Int = cinema.size

    fun displayCinema(cinema: List<Search>) {
        this.cinema.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(cinema)
            notifyItemRangeInserted(0, size)
        }
    }
}