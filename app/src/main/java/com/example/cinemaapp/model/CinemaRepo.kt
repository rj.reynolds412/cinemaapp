package com.example.cinemaapp.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CinemaRepo @Inject constructor(
    private val cinemaService: CinemaService,
) {


    suspend fun getCinema(cinema: String) = withContext(Dispatchers.IO) {
        cinemaService.getCinemaSearch(s = cinema)
    }

    suspend fun getCinemaDetailById(detail: String) = withContext(Dispatchers.IO) {
        cinemaService.getCinemaById(i = detail)
    }

    suspend fun getCinemaDetailByTitle(detail: String) = withContext(Dispatchers.IO) {
        cinemaService.getCinemaByTitle(t = detail)
    }

}