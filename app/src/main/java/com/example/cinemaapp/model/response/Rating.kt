package com.example.cinemaapp.model.response


import com.google.gson.annotations.SerializedName

data class Rating(
    @SerializedName("Source")
    val source: String,
    @SerializedName("Value")
    val value: String,
    @SerializedName("Error")
    val error: String
)