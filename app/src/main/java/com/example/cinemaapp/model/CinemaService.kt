package com.example.cinemaapp.model

import com.example.cinemaapp.model.response.CinemaDetailResponse
import com.example.cinemaapp.model.response.CinemaResponse
import com.example.cinemaapp.model.response.Search
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface CinemaService {

    companion object{
        const val BASE_URL = "https://www.omdbapi.com"
        const val apiKey = "48c3878e"



    }

    @GET(".")
    suspend fun getCinemaSearch(
        @Query("apiKey") apikey : String = apiKey,
        @Query("s") s : String,
    ) : CinemaResponse

    @GET(".")
    suspend fun getCinemaById(
        @Query("apiKey") apikey : String = apiKey,
        @Query("i") i : String ,
    ) : CinemaDetailResponse

    @GET(".")
    suspend fun getCinemaByTitle(
        @Query("t") t : String,
        @Query("apiKey") apikey : String = apiKey,
    ) : CinemaDetailResponse
}