package com.example.cinemaapp.model.response


import com.google.gson.annotations.SerializedName

data class CinemaResponse(
    @SerializedName("Response")
    var response:  String = "",
    @SerializedName("Search")
    val search: List<Search> = emptyList(),
    val totalResults:  String = "",
    @SerializedName("Error")
    val error:  String = ""
)